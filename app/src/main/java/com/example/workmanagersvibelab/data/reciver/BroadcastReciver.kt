package com.example.workmanagersvibelab.data.reciver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.example.workmanagersvibelab.data.CatFact
import com.example.workmanagersvibelab.data.service.APIService
import com.google.gson.Gson

class BroadcastReciver {
    class APIResponseReceiver : BroadcastReceiver() {
        private var catFact: CatFact? = null

        override fun onReceive(context: Context?, intent: Intent?) {
            val response = intent?.getStringExtra(APIService.EXTRA_API_RESPONSE)
            if (response != null) {
                catFact = Gson().fromJson(response, CatFact::class.java)
            }
            Toast.makeText(context, "Cat Fact: ${catFact?.fact}", Toast.LENGTH_LONG).show()
        }

        fun getCatFact(): CatFact? {
            return catFact
        }
    }
}