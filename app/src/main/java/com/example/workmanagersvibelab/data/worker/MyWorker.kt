package com.example.workmanagersvibelab.data.worker

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.work.Worker
import androidx.work.WorkerParameters
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject

class MyWorker(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {
    private var catFact: String? = null

    override fun doWork(): Result {
        try {
            val response = makeAPIRequest()
            catFact = response.getString("fact")

            val applicationContext = applicationContext
            Handler(Looper.getMainLooper()).post {
                Toast.makeText(applicationContext, catFact, Toast.LENGTH_SHORT).show()
            }

            return Result.success()
        } catch (e: Exception) {
            return Result.failure()
        }
    }

    private fun makeAPIRequest(): JSONObject {
        val url = "https://catfact.ninja/fact"
        val request = Request.Builder()
            .url(url)
            .build()

        val client = OkHttpClient()
        val response = client.newCall(request).execute()
        val responseBody = response.body?.string()

        return JSONObject(responseBody.toString())
    }
}