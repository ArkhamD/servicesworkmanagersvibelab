package com.example.workmanagersvibelab.data.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

class APIService : Service() {

    companion object {
        const val ACTION_API_RESPONSE = "com.example.api.reponse"
        const val EXTRA_API_RESPONSE = "api_response"
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        makeAPIRequest()
        return super.onStartCommand(intent, flags, startId)
    }

    @OptIn(DelicateCoroutinesApi::class)
    private fun makeAPIRequest() {
        GlobalScope.launch {
            val response = fetchDataFromAPI()
            sendBroadcastResponse(response)
        }
    }

    private suspend fun fetchDataFromAPI(): String {
        val client = OkHttpClient()
        val request = Request.Builder()
            .url("https://catfact.ninja/fact")
            .build()

        return withContext(Dispatchers.IO) {
            var response: Response? = null
            try {
                response = client.newCall(request).execute()
                response.body?.string() ?: ""
            } catch (e: Exception) {
                Log.e("APIService", "API request failed: ${e.message}")
                ""
            } finally {
                response?.close()
            }
        }
    }

    private fun sendBroadcastResponse(response: String) {
        val intent = Intent(ACTION_API_RESPONSE)
        intent.putExtra(EXTRA_API_RESPONSE, response)
        sendBroadcast(intent)
    }
}