package com.example.workmanagersvibelab

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.example.workmanagersvibelab.data.reciver.BroadcastReciver
import com.example.workmanagersvibelab.data.service.APIService
import com.example.workmanagersvibelab.data.worker.MyWorker
import com.example.workmanagersvibelab.ui.theme.WorkManagersVIBELABTheme

class MainActivity : ComponentActivity() {
    private var apiResponseReceiver = BroadcastReciver.APIResponseReceiver()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            WorkManagersVIBELABTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Screen(requestService = { makeAPIRequest() }, requestWorkManagers = { startWorkManager() })
                }
            }
        }
    }
    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter(APIService.ACTION_API_RESPONSE)
        registerReceiver(apiResponseReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(apiResponseReceiver)
    }

    private fun makeAPIRequest() {
        val intent = Intent(this, APIService::class.java)
        startService(intent)
    }

    private fun startWorkManager() {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val workRequest = OneTimeWorkRequestBuilder<MyWorker>()
            .setConstraints(constraints)
            .build()

        WorkManager.getInstance(this).enqueue(workRequest)
    }
}


@Composable
fun FirstScreen(
    modifier: Modifier = Modifier,
    requestService: () -> Unit,
    switchScreensClick: () -> Unit
) {
    Column(
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .padding(
                top = 50.dp,
                bottom = 50.dp
            )
    ) {
        Text(text = stringResource(R.string.services))
        Button(onClick = { requestService() }) {
            Text(text = stringResource(R.string.upload))
        }
        Button(onClick = { switchScreensClick() }) {
            Text(text = stringResource(R.string.to_second_screen))
        }
    }
}

@Composable
fun SecondScreen(
    modifier: Modifier = Modifier,
    requestWorkManagers: () -> Unit = {},
    switchScreensClick: () -> Unit
) {
    Column(
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .padding(
                top = 50.dp,
                bottom = 50.dp
            )
    ) {
        Text(text = stringResource(R.string.workmanagers))
        Button(onClick = { requestWorkManagers() }) {
            Text(text = stringResource(R.string.upload))
        }
        Button(onClick = { switchScreensClick() }) {
            Text(text = stringResource(R.string.to_first_screen))
        }
    }
}

@Composable
fun Screen(
    requestService: () -> Unit,
    requestWorkManagers: () -> Unit,
    modifier: Modifier = Modifier,
) {
    var firstScreen by remember { mutableStateOf(true) }

    when(firstScreen) {
        true -> FirstScreen(switchScreensClick = {firstScreen = !firstScreen}, requestService = requestService)
        else -> SecondScreen(switchScreensClick = {firstScreen = !firstScreen}, requestWorkManagers = requestWorkManagers)
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun GreetingPreview() {
    WorkManagersVIBELABTheme {
        FirstScreen(
            Modifier.fillMaxSize(),
            switchScreensClick = {},
            requestService = {}
        )
    }
}
